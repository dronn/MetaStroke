extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _draw():
	var mainNode=get_parent()
	if mainNode.strokeCount > 0:
		var lastStroke=mainNode.twoDStrokes[mainNode.twoDStrokes.size()-1]
		if(lastStroke.size()>1 and mainNode.recordStroke > 0):
			var drawColour=Color(1,0,0,1)
			for i in range(1, lastStroke.size() -1):
				draw_line(lastStroke[i-1], lastStroke[i], drawColour)

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
func _process(delta):
	#update()
	pass
