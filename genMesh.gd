extends Node3D
var modelPartSequence=[]
var cursor=null
var radius=1.0
var sides=6
var segments=6
var useSegmentOrigin=true
var segmentOrigin=Vector3(0,0,0)
var clickPos=Vector3(0,0,0)
var useClicPos=true
var useClicPosObjZ=true
var segmentLen=3
var dynamic_radius=true
var radius_start=1.2
var radius_end=0.1

var fakeZ=0
var drawOnObjects=false
var mainNode=null
# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	mainNode=get_parent()
	var mesh = MeshInstance3D.new()
	var cap= load("res://0.tscn")
	var capMesh=cap.instantiate()
	capMesh.transform.origin = Vector3(0, -1.6, 0)
	mesh.add_child(capMesh)

	cursor=cap.instantiate()
	cursor.transform.origin = Vector3(0, -1.6, 0)
	cursor.transform=cursor.transform.scaled(Vector3(1.5,1.5,1.5))
	#mesh.add_child(cursor)
	add_child(mesh)

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

	
func genMesh():
	var latestStroke=mainNode.threeDStrokes[mainNode.threeDStrokes.size()-1]
	var lastSegmentVec=Vector3(0,0,0)
	var currentSegementVec=Vector3(0,0,0)
	var currentSegementLength=0
	var orientation=Vector3(0,0,0)
	var mesh = MeshInstance3D.new()

	var currentPos=Vector3(0,0,0)
	var currentPosOld=Vector3(0,0,0)

	#segment bounding box
	var segmentBeginIdx=0
	var segmentEndIdx=0
	var segmentPoints=[]
	var freeform=true
	var SegIdx=0
	var segCount=0
	var rot=mainNode.threeDStrokesHeaders[mainNode.threeDStrokesHeaders.size()-1][1]
	var offset=mainNode.threeDStrokesHeaders[mainNode.threeDStrokesHeaders.size()-1][0]
	for i in range(latestStroke.size()):
		lastSegmentVec=currentSegementVec
		currentSegementVec.x=latestStroke[i].x-latestStroke[segmentBeginIdx].x
		currentSegementVec.y=latestStroke[i].y-latestStroke[segmentBeginIdx].y
		currentSegementVec.z=latestStroke[i].z-latestStroke[segmentBeginIdx].z
		currentSegementLength=currentSegementVec.length()
		if(currentSegementLength>segmentLen ||i== latestStroke.size()-1):
			segmentBeginIdx=i
			segCount=segCount+1
			currentSegementLength=0
			lastSegmentVec=Vector3(0,0,0)
			currentSegementVec=Vector3(0,0,0)
			
	segmentBeginIdx=0
	latestStroke=mainNode.threeDStrokes[mainNode.threeDStrokes.size()-1]
	lastSegmentVec=Vector3(0,0,0)
	currentSegementVec=Vector3(0,0,0)
	currentSegementLength=0
	
	for i in range(latestStroke.size()):
		lastSegmentVec=currentSegementVec

		currentSegementVec.x=latestStroke[i].x-latestStroke[segmentBeginIdx].x
		currentSegementVec.y=latestStroke[i].y-latestStroke[segmentBeginIdx].y
		currentSegementVec.z=latestStroke[i].z-latestStroke[segmentBeginIdx].z
		currentSegementLength=currentSegementVec.length()
		segmentPoints.append(latestStroke[i])
		#identify meshpart
		#if(currentSegementLength>1.75):
		if(currentSegementLength>segmentLen ||i== latestStroke.size()-1):
			SegIdx=SegIdx+1
			segmentEndIdx=i
			if(freeform):

				#freeform
				#print("freeform!")
				var cylinderMesh
				if(mainNode.currentBrush==mainNode.brushes.SIMPLE):
					cylinderMesh=mainNode.get_node("segment").duplicate()
				elif(mainNode.currentBrush==mainNode.brushes.PROFILE):
					cylinderMesh=mainNode.get_node("segmentWithProfiles").duplicate()
					if mainNode.recordedBaseProfiles.size()>0:
						cylinderMesh.baseProfiles.append(mainNode.recordedBaseProfiles[0])
					if mainNode.recordedProfiles.size()>0:
						cylinderMesh.profiles.append(mainNode.recordedProfiles[0])
				if dynamic_radius:
					var start_radius=lerp(radius_start, radius_end, SegIdx*1.0/segCount)
					var end_radius=lerp(radius_start, radius_end, (SegIdx+1)*1.0/segCount)
					if(end_radius < 0):
						end_radius=0
						
					cylinderMesh.start_radius=start_radius
					cylinderMesh.end_radius=end_radius
					cylinderMesh.dynamic_radius=true
				cylinderMesh.startRotation=rot
				orientation=cylinderMesh.genSegment(orientation, latestStroke[i]-latestStroke[segmentBeginIdx],segmentPoints,segments,sides, radius)
				
				
				if(SegIdx==1):
					currentPos=latestStroke[segmentBeginIdx]
					cylinderMesh.transform.origin = currentPos
					cylinderMesh.startVec=offset
				else:
					cylinderMesh.transform.origin = currentPos
					cylinderMesh.startVec=offset
				#cylinderMesh.transform.origin = (currentPos-offset)
				cylinderMesh.strokeIndex=mainNode.threeDStrokes.size()-1
				currentPos.x=latestStroke[i].x;
				currentPos.y=latestStroke[i].y;
				currentPos.z=latestStroke[i].z;
				modelPartSequence.append(cylinderMesh)
				mesh.add_child(cylinderMesh)
			
			currentSegementLength=0
			segmentPoints=[]
			segmentBeginIdx=i

	mesh.rotate_object_local(Vector3(0,1,0), rot)
	mesh.transform.origin=offset

	add_child(mesh)
