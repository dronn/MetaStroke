extends Node3D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var scrolled=0.0
enum brushes{SIMPLE, PROFILE, META}
var currentBrush=brushes.SIMPLE
var recordProfile=0
var recordBaseProfile=0
var recordedBaseProfiles=[]
var recordedProfiles=[]
var rotate=0
var recordStroke=0
var rayCast=0
var selected=null
const ray_length = 1000
var strokeCount=0
var newStroke=0
var twoDStrokes =[]
var twoDStrokesHeaders=[]
var threeDStrokes=[]
var threeDStrokesHeaders=[]
var objectsHitByStroke=[]
var meshNode=null
var saving=false
# Called when the node enters the scene tree for the first time.
func _ready():
	meshNode=get_node("genMesh")
	pass

	#
func gen3DStroke():
	var genNode=get_node("genStroke3D")
	genNode.drawOnObjects=meshNode.drawOnObjects
	var offset=Vector3(0,0,0)
	var phi=0
	var camHolder=get_node("CameraHolder")
	phi=camHolder.rotation.y
	print("#############")
	print(phi)
	if(selected!=null):
		offset=selected.global_transform.origin

		if meshNode.useClicPos:
			offset=meshNode.clickPos
		if meshNode.useSegmentOrigin:
			offset=meshNode.segmentOrigin
		meshNode.cursor.transform.origin=offset
	var fz=genNode.fakeZ.NONE
	if(not meshNode.fakeZ==0):
		fz=genNode.fakeZ.SIMPLE
	genNode.gen3DStroke(offset, phi,fz)

	
func boundingBoxCheck(vecToCheck, vecBox, margin =1 ):
	var vecBox_=Vector3(vecBox)
	var vecToCheck_=Vector3(vecToCheck)
	var result=true
	if vecToCheck.x <= 0 && vecBox.x <= 0:
		vecBox_.x=abs(vecBox.x)
		vecToCheck_.x=abs(vecToCheck.x)
	if vecToCheck.y <= 0 && vecBox.y <= 0:
		vecBox_.y=abs(vecBox.y)
		vecToCheck_.y=abs(vecToCheck.y)
	if vecToCheck.z <= 0 && vecBox.z <= 0:
		vecBox_.z=abs(vecBox.z)
		vecToCheck_.z=abs(vecToCheck.z)

	if vecToCheck_.x < (vecBox_.x + margin) and vecToCheck_.y < (vecBox_.y+margin) and vecToCheck_.z < (vecBox_.z + margin):
		result=false
	return result
	
func saveJs(filename, text):
	var js="var element = document.createElement('a');\n"
	js=js+"element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(\""+text.replace("\n", "\\n")+"\"));\n"
	js=js+"element.setAttribute('download', \""+filename+"\");\n"
	js=js+"element.style.display = 'none';\n"
	js=js+"document.body.appendChild(element);\n"
	js=js+"element.click();\n"
	js=js+"document.body.removeChild(element);\n"
	#JavaScript.eval(js)

	
func save():
	saving=true
	if OS.has_feature('JavaScript'):
		save3DStrokePLY("sketch")
		save3DMesh("sketch")
	else:
		var fileDialogue=get_node("ui/fileDialogue")
		fileDialogue.fileSave()
	


	
func save3DStroke(path="user://"):
	var filepath=path+"_3DStroke.txt"
	var file = FileAccess.open(filepath, FileAccess.WRITE)
	var saveStr=""
	for i in range (threeDStrokesHeaders.size()):
		pass
	for i in range(threeDStrokes.size()):
		var currentStroke=threeDStrokes[i]
		for j in range(threeDStrokes[i].size()):
			var nextVecStr=str(currentStroke[j])
			if j==0:
				saveStr= nextVecStr
			else:
				saveStr= saveStr+", "+nextVecStr
		saveStr=saveStr+";\n"
	file.store_string(saveStr)
	file.close()
func save3DStrokePLY(path="user://"):
	var filepath=path+"_3DStrokes.ply"
	var file = FileAccess.open(filepath, FileAccess.WRITE)
	var saveStr="ply\nformat ascii 1.0\nelement vertex "
	var vertexCount=0
	var edgeCount=0
	for i in range(threeDStrokes.size()):
		vertexCount=vertexCount+threeDStrokes[i].size()
	edgeCount=vertexCount-threeDStrokes.size()
	saveStr=saveStr+str(vertexCount)
	saveStr=saveStr+"\nproperty float x\nproperty float y\nproperty float z\n"
	saveStr=saveStr+"element face 0\n"
	saveStr=saveStr+"property list uchar uint vertex_indices\n"
	saveStr=saveStr+"element edge "
	saveStr=saveStr+str(edgeCount)
	saveStr=saveStr+"\nproperty int vertex1 \nproperty int vertex2\nend_header\n"
	for i in range(threeDStrokes.size()):
		var currentStroke=threeDStrokes[i]
		var rot=threeDStrokesHeaders[i][1]
		var offset=threeDStrokesHeaders[i][0]
		for j in range(threeDStrokes[i].size()):
			var vec=currentStroke[j]
			vec=vec.rotated(Vector3(0,1,0),rot)
			vec=vec+offset
			var nextVecStr=str(vec.x).pad_decimals(2)+ " "+str(vec.y).pad_decimals(2)+ " "+str(vec.z).pad_decimals(2)+"\n"
			saveStr= saveStr+nextVecStr
	var vertCounter=0
	for i in range(threeDStrokes.size()):
		for k in range(threeDStrokes[i].size()):
			vertCounter=vertCounter +1
			if((k==threeDStrokes[i].size()-1)):
				pass
			else:
				saveStr= saveStr+str(vertCounter-1)+" "+str(vertCounter)+"\n"
	file.store_string(saveStr)
	file.close()
	if OS.has_feature('JavaScript'):
		saveJs("sketch_3DStrokes.ply", saveStr)
	
func save3DMesh(path="user://"):
	var filepath=path+"_Mesh.ply"
	var file = FileAccess.open(filepath, FileAccess.WRITE)
	var saveStr="ply\nformat ascii 1.0\nelement vertex "
	var vertexCount=0
	var faceCount=0
	var indexOffset=0
	var indicesStr="3 "
	var modelPartSequence=meshNode.modelPartSequence
	for i in range(modelPartSequence.size()):
		vertexCount=vertexCount+modelPartSequence[i].array_quad_vertices.size()
		faceCount=faceCount+modelPartSequence[i].quadCount
	saveStr=saveStr+str(vertexCount)
	saveStr=saveStr+"\nproperty float x\nproperty float y\nproperty float z\nproperty float s\nproperty float t\n"
	saveStr=saveStr+"element face " + str(faceCount*2) 
	saveStr=saveStr+"\nproperty list uchar uint vertex_indices\n"
	saveStr=saveStr+"end_header\n"
	for i in range(modelPartSequence.size()):
		for j in range(modelPartSequence[i].array_quad_vertices.size()):
			var vec=modelPartSequence[i].array_quad_vertices[j]
			var strokeIndex=modelPartSequence[i].strokeIndex
			vec=vec+modelPartSequence[i].transform.origin
			var rot=threeDStrokesHeaders[strokeIndex][1]
			var offset=threeDStrokesHeaders[strokeIndex][0]
			vec = vec.rotated(Vector3(0,1,0), rot)
			vec=offset+vec
			var nextVecStr=str(vec.x).pad_decimals(2)+ " "+str(vec.y).pad_decimals(2)+ " "+str(vec.z).pad_decimals(2)+" "
			var uv=Vector2(0,0)
			for k in range(0, modelPartSequence[i].array_uvs.size()):
				if  modelPartSequence[i].array_uvs[k][0]== j:
					uv=modelPartSequence[i].array_uvs[k][1]
					break
			nextVecStr=nextVecStr+str(uv.x).pad_decimals(2)+ " "+str(uv.y).pad_decimals(2)+"\n"
			saveStr= saveStr+nextVecStr
		for j in range(modelPartSequence[i].array_quad_indices.size()):
			indicesStr=indicesStr+str(modelPartSequence[i].array_quad_indices[j]+indexOffset)+" "
			if(j%3==2):
				indicesStr=indicesStr+"\n3 "
		indexOffset=indexOffset+modelPartSequence[i].array_quad_vertices.size()
	saveStr=saveStr+indicesStr
	file.store_string(saveStr)
	file.close()
	if OS.has_feature('JavaScript'):
		saveJs("sketch_Mesh.ply", saveStr)
		
func save2DStroke(path="user://"):
	var filepath=path+"_2dStrokes.txt"
	var file = FileAccess.open(filepath, FileAccess.WRITE)
	var saveStr=""
	for i in range(twoDStrokes.size()):
		var currentStroke=twoDStrokes[i]
		saveStr=saveStr+"[ "
		saveStr=saveStr+str(twoDStrokesHeaders[i])
		saveStr=saveStr+" ] "
		for j in range(twoDStrokes[i].size()):
			var nextVecStr=str(currentStroke[j])
			saveStr= saveStr+", "+nextVecStr
		saveStr=saveStr+";\n"
	file.store_string(saveStr)
	file.close()

func load():
	var file = FileAccess.open("user://save_game.dat", FileAccess.READ)
	var content = file.get_as_text()
	file.close()
	return content

func rayCastFn(position, color=Color(0,0,1,1), setSelected=false, setCursor=false):
	if(selected != null):
		var material = StandardMaterial3D.new()
		material.albedo_color=Color(0.7,0.77,0.85,1)
		material.params_cull_mode=StandardMaterial3D.CULL_DISABLED
		selected.set_material_override(material)
		if(selected.has_method("setSegMaterial")):
			selected.setSegMaterial(material)
	var camera = get_viewport().get_camera_3d()
	var space_state = get_world_3d().direct_space_state
	var from = camera.project_ray_origin(position)
	var to = from + camera.project_ray_normal(position) * ray_length
	var query = PhysicsRayQueryParameters3D.create(from, to)
	query.collide_with_areas = true
	var result = space_state.intersect_ray(query)
	var parent = 0
	var resultNode=0
	if(result.has("collider")):
		#print(result.collider.get_path())
		var rayWorld=result.position
		if setCursor and meshNode.cursor!=null:
			meshNode.cursor.transform.origin=rayWorld
		resultNode=get_node(result.collider.get_path())
		parent=resultNode.get_parent()
		var material = StandardMaterial3D.new()
		material.albedo_color=color
		material.params_cull_mode=StandardMaterial3D.CULL_DISABLED
		parent.set_material_override(material)
		if(setSelected):
			selected=parent
			if(selected.has_method("setSegMaterial")):
				selected.setSegMaterial(material)

	return resultNode
	
	
func _process(delta):
	if Input.is_action_just_pressed("mouseRotate"):
		rotate=1
	elif Input.is_action_just_released("mouseRotate"):
		rotate=0
	elif Input.is_action_just_released("click"):
		if not saving:
			if recordStroke>0:
				recordStroke=0
				gen3DStroke()
				meshNode.genMesh()
			if recordBaseProfile>0:
				recordBaseProfile=0
			if recordProfile!=0:
				recordProfile=0
	elif Input.is_action_just_pressed("click"):
		if  recordProfile==0 and recordBaseProfile==0 and not saving:
			recordStroke=1
			newStroke=1
	elif Input.is_action_pressed("rightClick"):
		rayCast=1
	elif Input.is_action_just_released("scrollUp"):
		#scrolled=scrolled+0.5
		var camHolder=get_node("CameraHolder/CameraHolder2")
		var camP=camHolder.transform.origin
		camHolder.transform.origin.z=camHolder.transform.origin.z-0.5
	elif Input.is_action_just_released("scrollDown"):
		#scrolled=scrolled-0.5
		var camHolder=get_node("CameraHolder/CameraHolder2")
		var camP=camHolder.transform.origin
		camHolder.transform.origin.z=camHolder.transform.origin.z+0.5
		#
func _input(ev):
	if ev is InputEventKey and ev.keycode == KEY_S and ev.pressed  and not ev.is_echo():
		save()
	if ev is InputEventKey and ev.keycode == KEY_ESCAPE:
		get_tree().quit()
	if ev is InputEventKey and ev.keycode == KEY_Q:
		get_tree().quit()
	if ev is InputEventKey and ev.keycode == KEY_P and ev.pressed  and not ev.is_echo():
		#toggle through brushes
			if(currentBrush==brushes.SIMPLE):
				currentBrush=brushes.PROFILE
				print("use PROFILE brush")
				recordBaseProfile=0
			elif(currentBrush==brushes.PROFILE):
				currentBrush=brushes.SIMPLE
				print("use SIMPLE brush")
				recordBaseProfile=0
	if ev is InputEventKey and ev.keycode == KEY_O and ev.pressed and not ev.is_echo():
		if ev.is_ctrl_pressed():
			meshNode.useClicPos= not meshNode.useClicPos
		elif ev.is_shift_pressed():
			meshNode.useClicPosObjZ= not meshNode.useClicPosObjZ
		else:
			meshNode.drawOnObjects=not meshNode.drawOnObjects
			meshNode.useClicPosObjZ=not meshNode.useClicPosObjZ
	if ev is InputEventKey and ev.keycode == KEY_1 and ev.pressed and not ev.is_echo():
		if(currentBrush==brushes.PROFILE):
			recordBaseProfile=1
			print("new profileBaseStroke")
			newStroke=1
	if ev is InputEventKey and ev.keycode == KEY_2 and ev.pressed and not ev.is_echo():
		if(currentBrush==brushes.PROFILE):
			recordProfile=1
			print("new profileStroke")
			newStroke=1
	if ev is InputEventKey and ev.keycode == KEY_D and ev.pressed and not ev.is_echo():
		if(ev.is_ctrl_pressed()&& ev.is_shift_pressed()):
			if(meshNode.segments>2):
				meshNode.segments=meshNode.segments-1
		elif(ev.is_ctrl_pressed()):
			if(meshNode.sides>2):
				meshNode.sides=meshNode.sides-1
		else:
			if(meshNode.radius>0.2):
				meshNode.radius=meshNode.radius-0.1
				meshNode.radius_start=meshNode.radius
				meshNode.radius_end=meshNode.radius*0.3
				if(meshNode.segmentLen>1.0):
					meshNode.segmentLen=meshNode.segmentLen-0.3
	if ev is InputEventKey and ev.keycode == KEY_F and ev.pressed and not ev.is_echo():
		if(ev.is_ctrl_pressed()&& ev.is_shift_pressed()):
			meshNode.segments=meshNode.segments+1
		elif(ev.is_ctrl_pressed()):
			meshNode.sides=meshNode.sides+1
		else:
			meshNode.radius=meshNode.radius+0.1
			meshNode.radius_start=meshNode.radius
			meshNode.radius_end=meshNode.radius*0.3
			if(meshNode.segmentLen<10):
				meshNode.segmentLen=meshNode.segmentLen+0.3
	if ev is InputEventKey and ev.keycode == KEY_Z  and ev.pressed and not ev.is_echo():
		if meshNode.fakeZ==0:
			meshNode.fakeZ=1
		elif meshNode.fakeZ==1:
			meshNode.fakeZ=0
	if ev is InputEventKey and ev.keycode == KEY_0 and ev.pressed and not ev.is_echo():
			if meshNode.dynamic_radius:
				meshNode.dynamic_radius=false
			else:
				meshNode.dynamic_radius=true
	if ev is InputEventKey and ev.keycode == KEY_F1 and ev.pressed and not ev.is_echo():
		var helpScreen=get_node("ui/HelpScreen")
		helpScreen.visible=not helpScreen.visible
	if ev is InputEventKey and ev.keycode == KEY_RIGHT:
		var camHolder=get_node("CameraHolder")
		var rotVec=Vector3(0,1,0)
		camHolder.rotate(rotVec,PI*0.1)
	elif ev is InputEventKey and ev.keycode == KEY_LEFT:
		var camHolder=get_node("CameraHolder")
		var rotVec=Vector3(0,1,0)
		camHolder.rotate(rotVec,-PI*0.1)
	elif ev is InputEventKey and ev.keycode == KEY_UP:
		var camHolder=get_node("CameraHolder/CameraHolder2")
		var rotVec=Vector3(1,0,0)
		camHolder.rotate(rotVec,-PI*0.1)
	elif ev is InputEventKey and ev.keycode == KEY_DOWN:
		var camHolder=get_node("CameraHolder/CameraHolder2")
		var rotVec=Vector3(1,0,0)
		camHolder.rotate(rotVec,PI*0.1)
	elif ev is InputEventMouseMotion and rotate == 0 and recordStroke==0 and recordBaseProfile==0 and recordProfile==0:
		rayCast=0
		if(selected != null):
			var material = StandardMaterial3D.new()
			material.albedo_color=Color(0.7,0.77,0.85,1)
			material.params_cull_mode=StandardMaterial3D.CULL_DISABLED
			selected.set_material_override(material)
			if(selected.has_method("setSegMaterial")):
				selected.setSegMaterial(material)
		var camera = get_viewport().get_camera_3d()
		var space_state = get_world_3d().direct_space_state
		var from = camera.project_ray_origin(ev.position)
		var to = from + camera.project_ray_normal(ev.position) * ray_length
		var query = PhysicsRayQueryParameters3D.create(from, to)
		query.collide_with_areas = true

		var result = space_state.intersect_ray(query)
		if(result != {}):
			print(result)
		if(result.has("collider")):
			print(result.collider.get_path())
			var resultNode=get_node(result.collider.get_path())
			var parent=resultNode.get_parent()
			var material = StandardMaterial3D.new()
			
			meshNode.clickPos=result.position
			if meshNode.useClicPosObjZ:
				meshNode.clickPos.z=resultNode.global_transform.origin.z
			material.albedo_color=Color(1,0,0,1)
			material.params_cull_mode=StandardMaterial3D.CULL_DISABLED
			parent.set_material_override(material)
			var roughRadius =meshNode.radius
			if meshNode.dynamic_radius:
				roughRadius=meshNode.radius_start
			if parent.get_parent().has_method("getOriginAt"):
				meshNode.segmentOrigin=parent.get_parent().getOriginAt(result.position, roughRadius)
			else:
				meshNode.segmentOrigin=result.position
				

			
			selected=parent
			if(selected.has_method("setSegMaterial")):
				selected.setSegMaterial(material)
				
			#parent.set_material_override.albedo_color=Color(1, 0, 0, 1)
			#print(parent)
	elif ev is InputEventMouseMotion:
		if rotate >0:
			var mouseMotion=ev.relative
			var factor=mouseMotion.x
			var camHolder=get_node("CameraHolder")
			var rotVec=Vector3(0,1,0)
			if abs(factor) > 0.1:
				camHolder.rotate(rotVec,-factor*PI*0.005)
			else:
				rotVec=Vector3(1,0,0)
				factor=mouseMotion.y
				if abs(factor) > 0.2:
					camHolder.rotate_object_local(rotVec,-factor*PI*0.005)
			#factor=1.0
			#if mouseMotion.y < 0.0:
			#	factor=-1.0
			#camHolder=get_node("CameraHolder")
			#rotVec=Vector3(1,0,0)
			#camHolder.rotate(rotVec,factor*PI*0.01)
		elif recordStroke >0:
			var camera = get_viewport().get_camera_3d()
			if newStroke==1:
				newStroke=0
				var newStrokeData=[]
				var hitObjectList=[]
				newStrokeData.append(ev.position)
				print("new Stroke")
				twoDStrokesHeaders.append(camera.global_transform)
				#print(ev.relative)
				twoDStrokes.append(newStrokeData)
				strokeCount=strokeCount+1
				objectsHitByStroke.append(hitObjectList)
			else:
				twoDStrokes[twoDStrokes.size() -1].append(ev.position)
				var hit=null
				if ev.is_ctrl_pressed():
					hit=rayCastFn(ev.position)
				if(hit):
					var item=[]
					item.append(ev.position)
					item.append(hit)
					var currentHits=objectsHitByStroke[objectsHitByStroke.size()-1]
					if currentHits.size() > 0:
						var lastHit=currentHits[currentHits.size()-1]
						if (ev.position-lastHit[0]).length() > meshNode.segmentLen:
							objectsHitByStroke[objectsHitByStroke.size()-1].append(item)
					else:
						objectsHitByStroke[objectsHitByStroke.size()-1].append(item)
		elif recordBaseProfile>0:
			
			if newStroke==1:
				newStroke=0
				var newProfileData=[]
				print("new base Profile")
				recordedBaseProfiles.append(newProfileData)
			else:
				recordedBaseProfiles[recordedBaseProfiles.size()-1].append(ev.position)
		elif recordProfile>0:
			
			if newStroke==1:
				newStroke=0
				var newProfileData=[]
				print("new Profile")
				recordedProfiles.append(newProfileData)
			else:
				recordedProfiles[recordedBaseProfiles.size()-1].append(ev.position)
				#print(ev.relative)
	# Called every frame. 'delta' is the elapsed time since the previous frame.
	#func _process(delta):
	#	pass
