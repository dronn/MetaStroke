extends Node3D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
enum resampling_method {DROP, MEAN}

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func getStrokeLen(points):
	var strokeLen=0
	for i in range(1, points.size()):
		strokeLen=strokeLen+(points[i]-points[i-1]).length()
	return strokeLen

func getStrokeAt(points, at, axis=2, strokeLenParam=-1):
	var value=Vector2(0,0)
	var strokeLen=strokeLenParam
	var nearlyAt=0
	var idx=0
	
	if(axis==3):
		value=Vector3(0,0,0)
	#calculate length of the stroke
	if strokeLen==-1:
		strokeLen=0
		for i in range(1, points.size()):
			strokeLen=strokeLen+(points[i]-points[i-1]).length()
	
	for i in range(1,points.size()):
		if nearlyAt+(points[i]-points[i-1]).length()< at*strokeLen:
			nearlyAt=nearlyAt+(points[i]-points[i-1]).length()
			idx=idx+1
		else:
			break
	value= points[idx]
	var delta= at*strokeLen-nearlyAt
	
	var deltaPoints=0
	if(idx < points.size()-1):
		deltaPoints=(points[idx+1]-points[idx]).length()
		if(deltaPoints!=0):
			value=value+(points[idx+1]-points[idx])*(delta/deltaPoints)

	return value



func v3lerp(v1, v2, t):
	var x=lerp(v1.x, v2.x, t)
	var y=lerp(v1.y, v2.y, t)
	var z=lerp(v1.z, v2.z, t)
	return Vector3(x,y,z)
	
func resampleStroke2d(stroke, targetCount, method=resampling_method.MEAN ):
	var upsample=false
	if(targetCount > stroke.size()):
		upsample=true
	var targetArray=[]
	var valuecounter=0
	var currentValue=Vector2(0,0)
	print(stroke.size())
	if(not upsample):
		for i in range(stroke.size()):
			if valuecounter<floor(1.0*stroke.size()/targetCount):
				currentValue=currentValue+stroke[i]
				valuecounter=valuecounter+1
			else:
				if (floor(1.0*stroke.size()/targetCount))!=1.0*stroke.size()/targetCount:
					var pointVal=1.0*stroke.size()/targetCount-floor(1.0*stroke.size()/targetCount)
					if randf()<pointVal:
						currentValue=currentValue+stroke[i]
						valuecounter=valuecounter+1
						targetArray.append(currentValue/valuecounter*1.0)
						valuecounter=0
						currentValue=Vector2(0,0)
					else:
						targetArray.append(currentValue/valuecounter*1.0)
						valuecounter=0
						currentValue=Vector2(0,0)
						currentValue=currentValue+stroke[i]
						valuecounter=valuecounter+1
				else:
						targetArray.append(currentValue/valuecounter*1.0)
						valuecounter=0
						currentValue=Vector2(0,0)
						currentValue=currentValue+stroke[i]
						valuecounter=valuecounter+1
	if targetArray.size()< targetCount:
		targetArray.append(currentValue/valuecounter*1.0)
	else:
		for i in range(stroke.size()-1):
			for k in range(floor(targetCount*1.0/stroke.size())):
				if k==0:
					targetArray.append(stroke[i])
				elif(k==floor(targetCount*1.0/stroke.size())):
					var pointVal=targetCount*1.0/stroke.size()-floor(targetCount*1.0/stroke.size())
					if randf()<pointVal:
						var value= v3lerp(stroke[i], stroke[i+1], (k-0.5)/(targetCount*1.0/stroke.size()))
						targetArray.append(value)
					var value=v3lerp(stroke[i], stroke[i+1], k*1.0/(targetCount*1.0/stroke.size()))
					targetArray.append(value)
				else:
					var value=v3lerp(stroke[i], stroke[i+1], k*1.0/(targetCount*1.0/stroke.size()))
					targetArray.append(value)

						
	return targetArray


func morphStroke(stroke, targetStroke, factor):
	var numPoints=stroke.size()
	var strokeLen=getStrokeLen(stroke)
	var targetStrokeLen=getStrokeLen(targetStroke)
	var morphedStroke=[]
	
	for i in range (0, numPoints-1):
		var targetStrokeAt=getStrokeAt(targetStroke, i*1.0/(numPoints*1.0),2, targetStrokeLen)
		var newPoint= v3lerp(stroke[i], targetStrokeAt, factor)
		morphedStroke.append(newPoint)
