extends MeshInstance3D
var scaleZ=true
var scaleZfactor=30
var useHitObjects=true
var drawOnObjects=false

enum fakeZ{NONE, SIMPLE}

# Called when the node enters the scene tree for the first time.
func _ready():
	pass
func adapt2DVec(vec, scale=40):
	var projectResolution=Vector2(ProjectSettings.get_setting("display/window/size/viewport_width"), 
	ProjectSettings.get_setting("display/window/size/viewport_height"))
	var ratio =projectResolution.x*1.0/projectResolution.y
	var x=-1*(projectResolution.x-vec.x-projectResolution.x/2)*scale/projectResolution.x*ratio
	var y=projectResolution.y-vec.y*scale/projectResolution.y
	return Vector2(x,y)
func twoDLen(points):
	var firstPoint=points[0]
	var lastPoint=points[points.size()-1]
	var delta=lastPoint-firstPoint
	return delta.length()
func threeDLen(points):
	var firstPoint=points[0]
	var lastPoint=points[points.size()-1]
	var delta=lastPoint-firstPoint
	return delta.length()
func distToCamera(position):
	var camera=get_viewport().get_camera_3d()
	var p=Plane(camera.global_transform.basis.z,camera.global_transform.origin.length())
	var pDist=p.distance_to(position)

	
	return -pDist
func lookAheadSmoothZ(index, points2D, hitListP, hitList, relateTo=0, lookAhead=20):
	var value=0.0
	var actualLookAhead=lookAhead
	var hitListPNext=hitListP
	if points2D.size() -index+1 < lookAhead:
		lookAhead=points2D.size() -index+1 
	for i in range (index, actualLookAhead):
		for k in range(hitListP, hitList.size()):
				if hitList[k][0].x==points2D[i].x && hitList[k][0].y==points2D[i].y:
					print("Found Hit")
					hitListPNext=k
					var newValue=distToCamera(hitList[k][1].global_transform.origin)-relateTo
					value=value+newValue*(actualLookAhead-i)/(actualLookAhead*actualLookAhead)
	return [value, hitListPNext]
	
func getNextSection(index, points2D, hitListP, hitList):
	var value=0.0
	var hitListPNext=hitListP
	var sectionLen=1.0
	for i in range(index+1, points2D.size()):
		for k in range(hitListP, hitList.size()):
				if hitList[k][0].x==points2D[i].x && hitList[k][0].y==points2D[i].y:
					#print("Found new Section")
					hitListPNext=k
					sectionLen=i
					value=distToCamera(hitList[k][1].global_transform.origin)
					break;
	return  [value, hitListPNext, sectionLen]

func gen3DStroke(startVec=Vector3(0,0,0), rot=0, fz=fakeZ.NONE,all=false):
	var mainNode=get_parent()
	var camera=get_viewport().get_camera_3d()
	var deltaCam=abs(distToCamera(startVec))
	var startZ=distToCamera(startVec)
	var cameraT=camera.global_transform.origin
	var finalZ=startZ
	var lastSectionZ=startZ
	var sectionZ=startZ
	var sectionLen=1.0
	var lastSectionIndex=0
	var firstHit=true
	#print("starz_rel "+ String(startZ)+ " camera "+String(cameraT) + " startPos: "+ String(startVec))
	var zScale=1.0
	var oldZ=startZ
	if scaleZ:
		if(startVec!=Vector3(0,0,0)):
			zScale= 1.0*deltaCam/scaleZfactor
			if zScale<1.0:
				zScale=1.0
	var material = StandardMaterial3D.new()
	material.albedo_color=Color(0,1,0,1)
	material.params_cull_mode=StandardMaterial3D.CULL_DISABLED
	var currentPos=Vector3(0,0,0)
	var vertIndex=0
	var start=mainNode.twoDStrokes.size()-1
	if all:
		start=0
	if mainNode.strokeCount > 0:
		for i in range(start, mainNode.twoDStrokes.size()):
			vertIndex=0
			var hitList=[]
			var hitListP=0
			var finalPoint=null
			if(useHitObjects):
				hitList=mainNode.objectsHitByStroke[i]
			var surface_tool = SurfaceTool.new();
			surface_tool.begin(Mesh.PRIMITIVE_LINES);
			

			var currentStroke=mainNode.twoDStrokes[i]
			var oldPoint=adapt2DVec(currentStroke[0])
			currentPos=Vector3(0,0,0)
			
			if(useHitObjects):
				if(hitList.size()>0):
					if hitList[hitList.size()-1][0].x==currentStroke[currentStroke.size()-1].x:
						if hitList[hitList.size()-1][0].y==currentStroke[currentStroke.size()-1].y:
							finalPoint=hitList[hitList.size()-1][1].global_transform.origin
							finalZ=distToCamera(finalPoint)
#							print("FInal: "+String(finalPoint)+ String(startVec))
#							print("final Z: " +String(finalZ))
#							print("start Z: " +String(startZ))
							finalZ=startZ-finalZ
#							print("deltaZ: "+String(finalZ))
							if(abs(finalZ)> 15):
								finalPoint=null
								#print("final point too long!")
			
			var threeDStroke=[]
			for j in range(1, currentStroke.size()):
				#print(currentStroke[j])
				surface_tool.set_normal(Vector3(1, 1, -1));
				surface_tool.set_color(Color(1, 0, 0, 1));
				var current2D=adapt2DVec(currentStroke[j])
				var delta=Vector2(current2D.x-oldPoint.x, current2D.y-oldPoint.y)
				delta=delta*zScale
				var z=0
				if(fz==fakeZ.SIMPLE):
					z=min(delta.x,delta.y)
				elif(useHitObjects):
					if(hitList.size()>0):
						#var result=lookAheadSmoothZ(j, currentStroke,hitListP, hitList,currentPos.z+startZ)
						if(j-lastSectionIndex>=sectionLen):
							var result=getNextSection(j, currentStroke,hitListP, hitList)
							if(result[0]!=0):
								lastSectionIndex=j
								lastSectionZ=sectionZ
								sectionZ=lastSectionZ-result[0]
								hitListP=result[1]
								sectionLen=result[2]
								#sectionLen=currentStroke.size()/hitList.size()
								#print("new section: "+String(lastSectionZ)+" "+String(sectionZ)+" "+String(sectionLen)+ " "+String(hitList.size()))
						z=sectionZ/sectionLen
						#z=0
						if(z==0.0) and finalPoint!=null:
							z=finalZ/(currentStroke.size()-1)
							
							#print("step towards finalZ: "+String(z))
						
				if(drawOnObjects and j>5):

					var from = camera.project_ray_origin(currentStroke[j])
					var to = from + camera.project_ray_normal(currentStroke[j]) * mainNode.ray_length
					var directState = PhysicsServer3D.space_get_direct_state(camera.get_world_3d().get_space())
					var query = PhysicsRayQueryParameters2D.create(from, to)
					query.exclude = [self]
					var result = directState.intersect_ray(query)
					#var result = directState.intersect_ray(from, to, [self])
					if result.has("collider"): 
						var rayWorld=result.position
						var tmp_z=distToCamera(rayWorld)-oldZ
						z=tmp_z*-1
						oldZ=oldZ+tmp_z
						print(tmp_z)
					
					
								
				if(j==1):
					print("Vertex FirstZ: ", str(currentPos.z+z), "old: " + str(currentPos.z)+ "delta: " + str(z))
					
				var newVertex=Vector3(currentPos.x+delta.x, currentPos.y+delta.y, currentPos.z+z);
				vertIndex=vertIndex+1
				surface_tool.add_vertex(newVertex);
				surface_tool.add_index(vertIndex);
				vertIndex=vertIndex+1
				surface_tool.add_vertex(newVertex);
				surface_tool.add_index(vertIndex);
				threeDStroke.append(newVertex)
				currentPos.x=currentPos.x+delta.x
				currentPos.y=currentPos.y+delta.y
				currentPos.z=currentPos.z+z
				oldPoint=current2D

			if(vertIndex%2==0):
				surface_tool.add_vertex(Vector3(currentPos.x, currentPos.y, currentPos.z));

			mainNode.threeDStrokes.append(threeDStroke)
			var mesh_node = MeshInstance3D.new()
			surface_tool.set_material(material)
			mesh_node.mesh = surface_tool.commit()
			mesh_node.rotate_object_local(Vector3(0,1,0), rot)
			mainNode.threeDStrokesHeaders.append([startVec,rot])
			mesh_node.transform.origin=startVec
			add_child(mesh_node)
		#mesh=surface_tool.commit()
 
