extends Control

# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func fileSave():
	$FileDialog.popup()
	

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_FileDialog_file_selected(path):
	var mainNode=get_parent().get_parent()
	mainNode.save2DStroke(path)
	mainNode.save3DStroke(path)
	mainNode.save3DStrokePLY(path)
	mainNode.save3DMesh(path)
	mainNode.saving=false
	 # Replace with function body.
	 # Replace with function body.
