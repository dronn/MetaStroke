extends MeshInstance3D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var array_quad_vertices = [];
var array_quad_indices = [];
var array_uvs=[];
var sides=4
var segments=4
var centerPoints=[]
var dictionary_check_quad_vertices = {};
var SegMaterial= preload("res://objMaterial.tres")
var quadCount=0
var strokeIndex=0
var twist=0.0
var adapt_z=0.0
var dynamic_radius=false
var start_radius=0.5
var end_radius=1.5
var baseProfiles=[]
var profiles=[]
var baseProfiles_adapt=[]
var profiles_adapt=[]
var profiles_params=[]
var profileSymmetry=true
var divideStrokeEven=true
var strokeLen=0
var myRadius=1.0
var startVec=Vector3(0,0,0)
var startRotation=0
# Declare member variables here. Examples:
# var a = 2
# var b = "text"
enum resampling_method {DROP, MEAN}

func getStrokeLen(points):
	var strokeLen=0
	for i in range(1, points.size()):
		strokeLen=strokeLen+(points[i]-points[i-1]).length()
	return strokeLen

func getStrokeAt(points, at, axis=2, strokeLenParam=-1):
	var value=Vector2(0,0)
	var strokeLen=strokeLenParam
	var nearlyAt=0
	var idx=0
	
	if(axis==3):
		value=Vector3(0,0,0)
	#calculate length of the stroke
	if strokeLen==-1:
		strokeLen=0
		for i in range(1, points.size()):
			strokeLen=strokeLen+(points[i]-points[i-1]).length()
	
	for i in range(1,points.size()):
		if nearlyAt+(points[i]-points[i-1]).length()< at*strokeLen:
			nearlyAt=nearlyAt+(points[i]-points[i-1]).length()
			idx=idx+1
		else:
			break
	value= points[idx]
	var delta= at*strokeLen-nearlyAt
	
	var deltaPoints=0
	if(idx < points.size()-1):
		deltaPoints=(points[idx+1]-points[idx]).length()
		if(deltaPoints!=0):
			value=value+(points[idx+1]-points[idx])*(delta/deltaPoints)

	return value

func getOriginAt(targetPoint, targetRadius):
	var origin=targetPoint
	var closest=Vector3(0,0,0)
	var closestDist=closest.distance_to(targetPoint)
	var pointOnStroke
	var closestIdx=0
	#var startVec=global_transform.origin
	#var startVec=Vector3(0,0,0)
	if strokeLen==0 or targetPoint==Vector3(0,0,0):
		return targetPoint
	for i in range(100):
		pointOnStroke=getStrokeAt(centerPoints,i*1.0/100.0,3,strokeLen)
		pointOnStroke=pointOnStroke.rotated(Vector3(0,1,0),startRotation)
		pointOnStroke=pointOnStroke+startVec
		if pointOnStroke.distance_to(targetPoint) < closestDist:
			closest=pointOnStroke
			#closest=closest.rotated(Vector3(0,1,0),startOrientation)
			closest=closest
			closestIdx=i
			closestDist=pointOnStroke.distance_to(targetPoint)
	if targetRadius > 0.1*myRadius:
		if targetRadius> myRadius:
			origin=closest
		else:
			var lerpFactor=targetRadius/myRadius
			origin=v3lerp(targetPoint, closest, lerpFactor)

		
	return origin
	

func v3lerp(v1, v2, t):
	var x=lerp(v1.x, v2.x, t)
	var y=lerp(v1.y, v2.y, t)
	var z=lerp(v1.z, v2.z, t)
	return Vector3(x,y,z)
func resampleStroke2d(stroke, targetCount, method=resampling_method.MEAN ):
	var upsample=false
	if(targetCount > stroke.size()):
		upsample=true
	var targetArray=[]
	var valuecounter=0
	var currentValue=Vector2(0,0)
	print(stroke.size())
	if(not upsample):
		for i in range(stroke.size()):
			if valuecounter<floor(1.0*stroke.size()/targetCount):
				currentValue=currentValue+stroke[i]
				valuecounter=valuecounter+1
			else:
				if (floor(1.0*stroke.size()/targetCount))!=1.0*stroke.size()/targetCount:
					var pointVal=1.0*stroke.size()/targetCount-floor(1.0*stroke.size()/targetCount)
					if randf()<pointVal:
						currentValue=currentValue+stroke[i]
						valuecounter=valuecounter+1
						targetArray.append(currentValue/valuecounter*1.0)
						valuecounter=0
						currentValue=Vector2(0,0)
					else:
						targetArray.append(currentValue/valuecounter*1.0)
						valuecounter=0
						currentValue=Vector2(0,0)
						currentValue=currentValue+stroke[i]
						valuecounter=valuecounter+1
				else:
						targetArray.append(currentValue/valuecounter*1.0)
						valuecounter=0
						currentValue=Vector2(0,0)
						currentValue=currentValue+stroke[i]
						valuecounter=valuecounter+1
	if targetArray.size()< targetCount:
		targetArray.append(currentValue/valuecounter*1.0)
	else:
		for i in range(stroke.size()-1):
			for k in range(floor(targetCount*1.0/stroke.size())):
				if k==0:
					targetArray.append(stroke[i])
				elif(k==floor(targetCount*1.0/stroke.size())):
					var pointVal=targetCount*1.0/stroke.size()-floor(targetCount*1.0/stroke.size())
					if randf()<pointVal:
						var value= v3lerp(stroke[i], stroke[i+1], (k-0.5)/(targetCount*1.0/stroke.size()))
						targetArray.append(value)
					var value=v3lerp(stroke[i], stroke[i+1], k*1.0/(targetCount*1.0/stroke.size()))
					targetArray.append(value)
				else:
					var value=v3lerp(stroke[i], stroke[i+1], k*1.0/(targetCount*1.0/stroke.size()))
					targetArray.append(value)

						
	return targetArray
# Called when the node enters the scene tree for the first time.
func scaleBrushWithProfile2(segmentIndex, segmentCount, currentProfile=0):
	
	var x1=1.0
	var x2=1.0
	if(profileSymmetry):
		x1= getStrokeAt(profiles_adapt[currentProfile],1.0*segmentIndex/segmentCount,2,profiles_params[currentProfile][1]).x
		x2=x1
	else:
		var rel=profiles_params[currentProfile][0]/profiles_params[currentProfile][1]
		var atx1=rel*1.0*segmentIndex/segmentCount
		var atx2=rel+(1.0-rel)*segmentIndex/segmentCount
		x1=getStrokeAt(profiles_adapt[currentProfile],atx1,2,profiles_params[currentProfile][1]).x
		x2=getStrokeAt(profiles_adapt[currentProfile],atx2,2,profiles_params[currentProfile][1]).x
	
	var scale=Vector2(abs(x1), abs(x2*0.5))
	return scale

func scaleBrushWithProfile(segmentIndex, segmentCount, closed=false, currentProfile=0):
	var scale=Vector2(1.0,1.0)
	if closed and segmentIndex==0 or segmentIndex==segmentCount-1:
			var scale0=scaleBrushWithProfile2(0, segmentCount, currentProfile)
			var scaleMax=scaleBrushWithProfile2(segmentCount-1, segmentCount, currentProfile)
			scale.x=scale0.x+scaleMax.x/2.0
			scale.y=scale0.y+scaleMax.y/2.0
	else:
		scale=scaleBrushWithProfile2(segmentIndex, segmentCount, currentProfile)
	return scale

func _ready():
	pass
	 # Replace with function body.

func min_abs(x,y):
	var f =min(abs(x),abs(y))
	if f==abs(x) && x <0:
		f=-1.0*f
	if f==abs(y) && y <0:
		f=-1.0*f
	return f
func _add_or_get_vertex_from_array(vertex):
	if dictionary_check_quad_vertices.has(vertex) == true:
		return dictionary_check_quad_vertices[vertex];
	
	else:
		array_quad_vertices.append(vertex);
		
		dictionary_check_quad_vertices[vertex] = array_quad_vertices.size()-1;
		return array_quad_vertices.size()-1;
func processStroke(inputStroke, outputStroke):
	var projectResolution=Vector2(ProjectSettings.get_setting("display/window/size/viewport_width"), 
	ProjectSettings.get_setting("display/window/size/viewport_height"))
	var ratio =projectResolution.x*1.0/projectResolution.y
	var x
	var y
	var scale=1.0
	var starty=(projectResolution.y-inputStroke[0].y)/projectResolution.y
	var startx=(inputStroke[0].x)/projectResolution.x
	for i in range(inputStroke.size()):
		x=(inputStroke[i].x/projectResolution.x)-startx
		y=((projectResolution.y-inputStroke[i].y)/projectResolution.y)-starty
		outputStroke.append(Vector2(x,y))

func add_quad(point_1, point_2, point_3, point_4):
	
	var vertex_index_one = -1;
	var vertex_index_two = -1;
	var vertex_index_three = -1;
	var vertex_index_four = -1;
	quadCount=quadCount+1
	
	vertex_index_one = _add_or_get_vertex_from_array(point_1);
	vertex_index_two = _add_or_get_vertex_from_array(point_2);
	vertex_index_three = _add_or_get_vertex_from_array(point_3);
	vertex_index_four = _add_or_get_vertex_from_array(point_4);
	#print("quad:"+String(vertex_index_one)+String(vertex_index_two)+String(vertex_index_three)+String(vertex_index_four))
	array_quad_indices.append(vertex_index_one)
	array_quad_indices.append(vertex_index_two)
	array_quad_indices.append(vertex_index_three)
	
	array_quad_indices.append(vertex_index_one)
	array_quad_indices.append(vertex_index_three)
	array_quad_indices.append(vertex_index_four)
func setSegMaterial(material):
	var mesh_node2=get_child(1)
	if(mesh_node2!=null):
		mesh_node2.set_material_override(material)

func genSegment(inOrientation, outPos, points=[], segments=1, sides=4, radius=1.0):
	var inPos=Vector3(0,0,0)
	var vertices=[]
	var phi=PI/2
	var pointsSegStart=0
	var pointsSegEnd=0
	var plen=0
	var outOrientation=(Vector2(0,1))
	var baseProfileAdapted=[]
	var profileAdapted=[]
	var tmpRadius=radius
	strokeLen=getStrokeLen(points)
	var oldPoint= points[0]
	var currentPoint=points[0]
	myRadius=radius
	if dynamic_radius:
		myRadius=0.5*(start_radius+end_radius)
	
	if(points.size()>1):
		centerPoints=points

	if baseProfiles.size()>0:
		var verticesPerSide=baseProfiles.size()*1.0/sides
		baseProfileAdapted=resampleStroke2d(baseProfiles[0], sides)
		var maxLen=0
		var center=Vector2(0,0)
		for i in range(baseProfileAdapted.size()):
			center=center+baseProfileAdapted[i]
		center=center*1.0/baseProfileAdapted.size()
		for i in range(baseProfileAdapted.size()):
			baseProfileAdapted[i]=baseProfileAdapted[i]-center
		for i in range(baseProfileAdapted.size()):
			if(baseProfileAdapted[i].length()> maxLen):
				maxLen=baseProfileAdapted[i].length()
				
		for i in range(baseProfileAdapted.size()):
			baseProfileAdapted[i]=baseProfileAdapted[i]/maxLen
		
	for i in range(1,points.size()):
		plen=plen+(points[i]-points[i-1]).length()
	var step=plen/segments
	
	if profiles.size()>0:
		var maximumy=0
		var maximumx=0
		var maxyPos=0
		var maxySum=0
		var profileLen=0
		processStroke(profiles[0], profileAdapted)
		for i in range(profileAdapted.size()):
			if abs(profileAdapted[i].y) > maximumy:
				maximumy=abs(profileAdapted[i].y)
				maxyPos=i
			if abs(profileAdapted[i].x) > maximumx:
				maximumx=abs(profileAdapted[i].x)
		if abs(profileAdapted[profileAdapted.size()-1].y) < maximumy:
			profileSymmetry=false
		print("Profile Symmetry: "+str(profileSymmetry))
		if profileSymmetry:
			for i in range(profileAdapted.size()):
				profileAdapted[i]=Vector2(abs(profileAdapted[i].x)/maximumx, abs(profileAdapted[i].y)/maximumy)
				if i < maxyPos && i> 1:
					maxySum=maxySum+(profileAdapted[i]-profileAdapted[i-1]).length()
				if(i>1):
					profileLen=profileLen+(profileAdapted[i]-profileAdapted[i-1]).length()
			#processSymmetricProfile(profileAdapted)
		else:
			for i in range(profileAdapted.size()):
				if i < maxyPos:
					profileAdapted[i]=Vector2(abs(profileAdapted[i].x)/maximumx, abs(profileAdapted[i].y)/maximumy)
					if i>1:
						maxySum=maxySum+(profileAdapted[i]-profileAdapted[i-1]).length()
				else:
					profileAdapted[i]=Vector2(abs(profileAdapted[i].x)*2.0/maximumx, abs(profileAdapted[i].y)/maximumy)
				
				if(i>1):
					profileLen=profileLen+(profileAdapted[i]-profileAdapted[i-1]).length()
		profiles_adapt.append(profileAdapted)
		profiles_params.append([maxySum, profileLen])
		#print("test brush:")
		#for i in range(segments):
		#	print(String(i)+" "+String(scaleBrushWithProfile(i, segments+1)))
	
	
	var surface_tool = SurfaceTool.new();
	surface_tool.begin(Mesh.PRIMITIVE_TRIANGLES);
	
	var vertIndex=0

	for i in range(segments+1):
		var start=0
		var end=0
		var startIdx=0
		var endIdx=0
		var scale=null
		tmpRadius=radius
		if(dynamic_radius):
			tmpRadius=lerp(start_radius, end_radius*1.0, i*1.0/(segments+1))
		if(profiles_adapt.size()>0):
			if profileSymmetry:
				#tmpRadius=sin((2*PI/segments)*i)
				tmpRadius=tmpRadius*((scaleBrushWithProfile(i, segments+1).x)+start_radius)
				#print(String(i)+" "+String(tmpRadius))
			else:
				scale=scaleBrushWithProfile(i, segments+1)
				#print(String(i)+" "+String(scale))
		if(divideStrokeEven):
			oldPoint=currentPoint
			currentPoint=getStrokeAt(points, i*1.0/(segments+1), 3, strokeLen)
			start=oldPoint
			end=currentPoint
		else:
			var segLen=0
			for k in range(pointsSegStart,points.size()):
				segLen=(points[k]-points[k-1]).length()
				if segLen>=step:
					pass
				else:
						pointsSegEnd=k
			#start=points[pointsSegStart]-points[0]
			#end=points[pointsSegEnd]-points[0]
			start=inPos
			end=outPos



		
		var lerpCenterPoint2=v3lerp(inPos, outPos, (i*1.0)/(segments*1.0))
		var index=i
		if index > points.size()-1:
			index=points.size()-1
		var lerpCenterPoint=points[index]-points[0]
		if(divideStrokeEven):
			lerpCenterPoint=currentPoint-points[0]
		if i==segments:
			lerpCenterPoint=outPos



		for j in range(sides):

			var x=sin((2*PI/sides)*j)*tmpRadius
			var z=cos((2*PI/sides)*j)*tmpRadius
			var y=0#(5/segments)*i
			if baseProfileAdapted.size()==sides:
				x=baseProfileAdapted[j].x*tmpRadius
				z=baseProfileAdapted[j].y*tmpRadius
		
			if(scale!=null):
				x=x*(scale.x+start_radius)
				z=z*(scale.y+start_radius)
			var newVertex=Vector3(x,y,z)
			if(twist!=0):
				newVertex=newVertex.rotated(Vector3(0,1,0), twist*i)
			var dvec=0
			if inOrientation!=Vector3(0,0,0) and i==0:
				dvec=inOrientation
			else:
				dvec = (Vector3(end.x, end.y, end.z) - Vector3(start.x, start.y, start.z)).normalized()
			outOrientation=dvec

			
			phi= dvec.angle_to (Vector3(0,1,0))

			var rotVec=Vector3(0,0,1)
			if(dvec.x<0):
				phi=-phi
			newVertex=newVertex.rotated(rotVec,-phi)
			
			if(adapt_z>0 ):
				var factor=-i*i*0.25+segments
				if factor<0:
					factor=0
				var new_z=min_abs(lerpCenterPoint.x, lerpCenterPoint.y)*adapt_z*factor
				lerpCenterPoint.z=new_z
			newVertex=newVertex +lerpCenterPoint;
			
			pointsSegStart=pointsSegEnd
			
			vertices.append(newVertex)
			
			var vertIndexUV=_add_or_get_vertex_from_array(newVertex)
			var uv=Vector2(i*1.0/(segments+1), j*1.0/sides)
			var uvItem=[vertIndexUV, uv]
			array_uvs.append(uvItem)
			if(i>0 ):
				if(j==0):
					pass
				else:
					add_quad(vertices[vertices.size()-2], vertices[vertices.size()-1],
						vertices[vertices.size()-sides-1], vertices[vertices.size()-sides-2])
					if(j==sides-1):
						add_quad(vertices[vertices.size()-1], vertices[vertices.size()-sides],
							vertices[vertices.size()-2*sides], vertices[vertices.size()-sides-1])
	var counter=0
	for vertex in array_quad_vertices:
		surface_tool.set_uv(array_uvs[counter][1])
		surface_tool.add_vertex(vertex);
		counter=counter+1
	for index in array_quad_indices:
		surface_tool.add_index(index);
		
	surface_tool.generate_normals();
	var mesh_node = MeshInstance3D.new()

	var material = SegMaterial

	surface_tool.set_material(material)
	mesh_node.mesh = surface_tool.commit()
	add_child(mesh_node)
	var collisionShape=get_node("StaticBody3D/CollisionShape3D")
	mesh_node.create_convex_collision()
	collisionShape.set_shape(mesh_node)
	return outOrientation
	
	
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
