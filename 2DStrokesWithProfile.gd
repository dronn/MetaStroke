extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _draw():
	var mainNode=get_parent()
	if mainNode.recordedBaseProfiles.size() > 0:
		var lastStroke=mainNode.recordedBaseProfiles[mainNode.recordedBaseProfiles.size()-1]
		if(lastStroke.size()>1 and mainNode.recordBaseProfile > 0):
			var drawColour=Color(0,0,1,1)
			for i in range(1, lastStroke.size() -1):
				draw_line(lastStroke[i-1], lastStroke[i], drawColour)
	if mainNode.recordedProfiles.size() > 0:
		var lastStroke=mainNode.recordedProfiles[mainNode.recordedProfiles.size()-1]
		if(lastStroke.size()>1 and mainNode.recordProfile > 0):
			var drawColour=Color(0.5,0,1,1)
			for i in range(1, lastStroke.size() -1):
				draw_line(lastStroke[i-1], lastStroke[i], drawColour)
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
func _process(delta):
	queue_redraw()
